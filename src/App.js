import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Container,
  ListGroup,
  Pagination,
  Form,
  Button,
  Navbar,
  Nav,
  Modal,
} from 'react-bootstrap';
import './Styles/App.css';
import Task from './Components/Task';
import Register from './Components/Register';
import Login from './Components/Login';
import './Styles/theme-light.css';

function App() {
  const [tasks, setTasks] = useState([]);
  const [taskTitle, setTaskTitle] = useState('');
  const [taskDescription, setTaskDescription] = useState('');
  const [taskPriority, setTaskPriority] = useState('Низкий');
  const [taskStatus, setTaskStatus] = useState('В работе');
  const [currentPage, setCurrentPage] = useState(1);
  const [showRegisterModal, setShowRegisterModal] = useState(false);
  const [showLoginModal, setShowLoginModal] = useState(false);
  

  const tasksPerPage = 5;

  const ITEMS_PER_PAGE = 5;

  const startIndex = (currentPage - 1) * ITEMS_PER_PAGE;
  const endIndex = startIndex + ITEMS_PER_PAGE;
  const tasksToShow = tasks.slice(startIndex, endIndex);

  const indexOfLastTask = currentPage * tasksPerPage;
  const indexOfFirstTask = indexOfLastTask - tasksPerPage;

  const currentTasks = tasks.slice(indexOfFirstTask, indexOfLastTask);

  const handleRegister = (userData) => {
    // Выполните логику регистрации
    // Например, отправьте данные на сервер и обработайте ответ
    console.log('Регистрация пользователя:', userData);
    setShowRegisterModal(false); // Закрываем модальное окно после регистрации
  };

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(tasks.length / tasksPerPage); i++) {
    pageNumbers.push(i);
  }

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleAddTask = () => {
    if (taskTitle.trim() !== '' && taskDescription.trim() !== '') {
      const newTask = {
        id: Date.now(),
        title: taskTitle,
        description: taskDescription,
        priority: taskPriority, // Добавляем приоритет
        status: taskStatus, // Добавляем статус
      };
      setTasks([...tasks, newTask]);
      setTaskTitle('');
      setTaskDescription('');
      setTaskPriority(''); // Сбрасываем приоритет после добавления задачи
      setTaskStatus('В работе'); // Сбрасываем статус после добавления задачи
    }
  };

  const handleDeleteTask = (taskId) => {
    const updatedTasks = tasks.filter((task) => task.id !== taskId);
    setTasks(updatedTasks);
  };

  const handleEditTask = (taskId, newTitle, newDescription, newPriority, newStatus) => {
    const updatedTasks = tasks.map((task) =>
      task.id === taskId
        ? {
            ...task,
            title: newTitle,
            description: newDescription,
            priority: newPriority,
            status: newStatus,
          }
        : task,
    );
    setTasks(updatedTasks);
  };

  const handleLogin = (userData) => {
    // Выполните логику аутентификации
    console.log('Пользователь вошел:', userData);
    setShowLoginModal(false);
  };

  const handleRegistrationClick = () => {
    setShowRegisterModal(true);
    setShowLoginModal(false); // Закрываем окно входа при открытии окна регистрации
  };

  const handleModalsClick = () => {
    setShowRegisterModal(false);
    setShowLoginModal(true);
  };

  const handlePriorityChange = (taskId, newPriority) => {
    const updatedTasks = tasks.map((task) =>
      task.id === taskId ? { ...task, priority: newPriority } : task,
    );
    setTasks(updatedTasks);
  };

  const handleStatusChange = (taskId, newStatus) => {
    const updatedTasks = tasks.map((task) =>
      task.id === taskId ? { ...task, status: newStatus } : task,
    );
    setTasks(updatedTasks);
  };

  return (
    <div>
      <Navbar bg="primary" expand="lg">
        <Container>
          <Navbar.Brand href="#home">Список задач</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#home">Главная</Nav.Link>
            </Nav>
            <Nav>
              <Nav.Link onClick={() => setShowRegisterModal(true)}>Регистрация</Nav.Link>
              <Nav.Link onClick={() => setShowLoginModal(true)}>Вход</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container className="mt-5">
        <h1>Список задач</h1>
        <Form>
          <Form.Group>
            <Form.Label>Заголовок задачи</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              value={taskTitle}
              onChange={(e) => setTaskTitle(e.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Описание задачи</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              value={taskDescription}
              onChange={(e) => setTaskDescription(e.target.value)}
            />
          </Form.Group>
          <Button variant="primary" onClick={handleAddTask}>
            Добавить задачу
          </Button>
        </Form>
        <ListGroup className="mt-3">
          {currentTasks.map((task) => (
            <ListGroup.Item key={task.id}>
              <Task
                task={task}
                onDelete={handleDeleteTask}
                onEdit={handleEditTask}
                onPriorityChange={handlePriorityChange} // Передаем функцию handlePriorityChange
                onStatusChange={handleStatusChange}
              />
            </ListGroup.Item>
          ))}
        </ListGroup>
        <Pagination className="mt-3">
          {pageNumbers.map((pageNumber) => (
            <Pagination.Item
              key={pageNumber}
              active={pageNumber === currentPage}
              onClick={() => handlePageChange(pageNumber)}>
              {pageNumber}
            </Pagination.Item>
          ))}
        </Pagination>
      </Container>
      <Modal centered show={showRegisterModal} onHide={() => setShowRegisterModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Регистрация</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Register onRegister={handleRegister} onLoginClick={handleModalsClick} />{' '}
          {/* Передаем функцию регистрации */}
        </Modal.Body>
      </Modal>
      <Modal centered show={showLoginModal} onHide={() => setShowLoginModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Вход в систему</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Login onLogin={handleLogin} onRegisterClick={handleRegistrationClick} />{' '}
          {/* Передаем функцию входа */}
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default App;
