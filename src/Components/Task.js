import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import '../Styles/Task.css';

function Task({ task, onDelete, onEdit, onPriorityChange, onStatusChange }) {
  const [showEditModal, setShowEditModal] = useState(false);
  const [editedTitle, setEditedTitle] = useState(task.title);
  const [editedDescription, setEditedDescription] = useState(task.description);
  const [taskPriority, setTaskPriority] = useState(task.priority);
  const [taskStatus, setTaskStatus] = useState(task.status);

  const [expanded, setExpanded] = useState(false);
  // const [isExpanded, setIsExpanded] = useState(false);

  const handleExpand = () => {
    setExpanded(!expanded);
  };

  // const toggleExpanded = () => {
  //   setIsExpanded(!isExpanded);
  // };
  

  const handleEditModalShow = () => {
    setEditedTitle(task.title);
    setEditedDescription(task.description);
    setTaskPriority(task.priority);
    setTaskStatus(task.status);
    setShowEditModal(true);
  };

  const handleEditModalClose = () => {
    setShowEditModal(false);
  };

  const handleEdit = () => {
    onEdit(task.id, editedTitle, editedDescription, taskPriority, taskStatus);
    onPriorityChange(task.id, taskPriority);
    onStatusChange(task.id, taskStatus);
    handleEditModalClose();
  };

  return (
    <div className="task-item">
      <div>
        <h2>Задача:</h2>
        <h5 className="task-title">{task.title}</h5>
      </div>
      <div>
      <h4>Описание:</h4>
      <p className={`task-description ${expanded ? 'expanded' : ''}`}>{task.description}</p>
      <Button
        variant="link"
        size="sm"
        onClick={handleExpand}
        className={`expand-button ${expanded ? 'expanded' : ''}`}>
        {expanded ? 'Скрыть' : 'Подробнее'}
      </Button>
      </div>
      <div className="task-actions">
        <Button
          variant={
            taskPriority === 'Высокий'
              ? 'danger'
              : taskPriority === 'Средний'
              ? 'warning'
              : 'success'
          }
          size="sm"
          onClick={() => {
            const newPriority =
              taskPriority === 'Высокий'
                ? 'Средний'
                : taskPriority === 'Средний'
                ? 'Низкий'
                : 'Высокий';
            setTaskPriority(newPriority);
            onPriorityChange(task.id, newPriority);
          }}>
          Приоритет: {taskPriority}
        </Button>
        <Button
          variant={taskStatus === 'В работе' ? 'warning' : 'success'}
          size="sm"
          onClick={() => {
            const newStatus = taskStatus === 'В работе' ? 'Выполнено' : 'В работе';
            setTaskStatus(newStatus);
            onStatusChange(task.id, newStatus); // Вызываем onStatusChange
          }}>
          Статус: {taskStatus}
        </Button>
        <Button variant="secondary" size="sm" onClick={handleEditModalShow}>
          Изменить
        </Button>
        <Button variant="danger" size="sm" onClick={() => onDelete(task.id)}>
          Удалить
        </Button>
      </div>

      {/* Модальное окно для редактирования */}
      <Modal size="lg" centered show={showEditModal} onHide={handleEditModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Изменить задачу</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Заголовок задачи</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              value={editedTitle}
              onChange={(e) => setEditedTitle(e.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Описание задачи</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              value={editedDescription}
              onChange={(e) => setEditedDescription(e.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Приоритет задачи</Form.Label>
            <Form.Control
              as="select"
              value={taskPriority}
              onChange={(e) => setTaskPriority(e.target.value)}>
              <option value="Высокий">Высокий</option>
              <option value="Средний">Средний</option>
              <option value="Низкий">Низкий</option>
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Статус задачи</Form.Label>
            <Form.Control
              as="select"
              value={taskStatus}
              onChange={(e) => setTaskStatus(e.target.value)}>
              <option value="В работе">В работе</option>
              <option value="Выполнено">Выполнено</option>
            </Form.Control>
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleEditModalClose}>
            Отмена
          </Button>
          <Button variant="primary" onClick={handleEdit}>
            Сохранить изменения
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default Task;
