import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

function Login({ onLogin, onRegisterClick }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    // Выполните логику аутентификации
    const userData = { username, password };
    onLogin(userData);
  };

  return (
    <Form>
      <Form.Group controlId="formBasicUsername">
        <Form.Label>Имя пользователя</Form.Label>
        <Form.Control
          type="text"
          placeholder="Введите имя пользователя"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="formBasicPassword">
        <Form.Label>Пароль</Form.Label>
        <Form.Control
          type="password"
          placeholder="Введите пароль"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Group>

      <Button variant="primary" onClick={handleLogin}>
        Войти
      </Button>
      <div className='mt-2'>
        <Button variant="secondary" className="ml-2" onClick={onRegisterClick}>
          Регистрация
        </Button>
      </div>
    </Form>
  );
}

export default Login;
