import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

function Register({ onRegister, onLoginClick }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleRegister = () => {
    // Выполните логику регистрации
    const userData = { username, password };
    onRegister(userData);
  };

  return (
    <Form>
      <Form.Group controlId="formBasicUsername">
        <Form.Label>Имя пользователя</Form.Label>
        <Form.Control
          type="text"
          placeholder="Введите имя пользователя"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="formBasicPassword">
        <Form.Label>Пароль</Form.Label>
        <Form.Control
          type="password"
          placeholder="Введите пароль"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Group>

      <Button variant="primary" onClick={handleRegister}>
        Зарегистрироваться
      </Button>
      <div className="mt-2">
        <Button variant="secondary" className="ml-2" onClick={onLoginClick}>
          Вход
        </Button>
      </div>
    </Form>
  );
}

export default Register;
